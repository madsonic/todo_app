#####Name: Gerald Ng
#####Matric no.: A0124312Y

#Todo manager

This app is available at this [link] and source code at [bitbucket]

##Basic use case

###CRUD to-dos
User can create, read, update and delete their to do list.
Interaction of text is done thru a form interface.
Marking it as done which changes the color of the todo.

###Categorization (WIP)
By append hash-tags (#some_category) to their to-dos, user can group their tasks according to the tags.

###Search (WIP)
In order to be able to quickly find your to-dos once there are lots of them, a search bar is available. User can search by:

1. content
2. hash tag
3. completion status

###Sessions/Users login
Allow sign up of accounts and login management

###Execution
Keeping RESTful api and MVC architecture in mind, the following items will be done in order. TDD will be attempted 

1. CRUD of to-do items
2. Search function 
3. User accounts
4. Handling multiple category

##New knowledge
1. Rails of course
2. Sass
3. MVC
4. REST api
5. TDD
6. deployment on Heroku

##Some comments
Firstly, acknowledgement. The above mentioned knowledge has been acquired through Michael Hartl's [book]. Kudos to him. Excellent book that is well structured and clear. And of course varied sources from the internet. Thus certain things in the app are done in a similar fashion to the tutorial app from the book. That said, conscious attempt has been made to minimise referral to his code during development.

Currently the app only supports basic CRUD operations on the to-do items. Basic styling to improve the overall look and feel of the app as well as to keep the to do items organised after all a to-do manager is suppose to improve your productivity so how can you get organised if the to-do items aren't

This app still has a long way to go. Some things that should be done has been mentioned above. Hopefully I would be able to execute them well. 

I would like to try out TDD for subsequent part of the development. I know this is a very bad practice to have as testing should be done right from the start if it is to be done at all. But due to time constraint, most effort has been put on realising the objective stated in the assignment. Although I have a rough idea of how testing works, I still do not understand why is there an ongoing debate about why testing is bad. See [Rails' creator post about this]

All in all the experience with Rails has been great so far. It really does promote coding happiness as promised by Rails. The language construct of Ruby and Rails is simply elegant. Despite the fact that I only know a limited number of programming languages, I feel that they are really empowering and are devoted to help programmers get thing done.

[link]: https://ancient-coast-6669.herokuapp.com/
[bitbucket]: https://bitbucket.org/madsonic/todo_app
[book]: https://www.railstutorial.org/book/
[Rails' creator post about this]: http://david.heinemeierhansson.com/2014/tdd-is-dead-long-live-testing.html