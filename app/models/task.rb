class Task < ActiveRecord::Base
  after_initialize :status_default

  def status_default
    self.status ||= false
  end
end
