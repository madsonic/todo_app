class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by email: params[:session][:email].downcase
    if user && user.authenticate(params[:session][:password])
      # on successful login redirect to user profile
      log_in user
      redirect_to root_path
    else
      # error message
      flash.now[:danger] = 'Invalid email/password'
      render 'new'
    end
  end

  def destroy
    if logged_in?
      log_out
    # reset_session
      flash[:success] = 'Logged out successfully'
      redirect_to root_path
    else
      flash[:warning] = 'You were not logged in'
      redirect_to root_path
    end
  end
end
