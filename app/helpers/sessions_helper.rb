module SessionsHelper
# Logs in the given user
  def log_in(user)
    session[:user_id] = user.id
  end

  # Returns current logged-in user. nil if none
  def current_user
    # find by id
    @current_user ||= User.find_by(id: session[:user_id])
  end

  # Returns true if user is logged in
  def logged_in?
    !current_user.nil?
  end

  # log out current logged in user
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end
end
