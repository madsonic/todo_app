require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = users(:one)
  end

  test 'name should be present' do
    @user.name = '     '
    assert_not @user.valid?
  end

  test 'email should be present' do
    @user.email = '     '
    assert_not @user.valid?
  end

  test 'name should have maximum length' do
    @user.name = 'a' * 56
    assert_not @user.valid?
  end

  test 'email should have maximum length' do
    @user.email = 'a' * 244 + '@example.com'
    assert_not @user.valid?
  end

  test 'email should be downcase' do
    @user.save
    user_dup = @user.dup
    user_dup.email = user_dup.email.downcase
    assert_not user_dup.valid?
  end

  test 'email validation should reject invalid emails' do
    invalid_email = %w[foo@example,com
                       foo.com
                       susan@bar_baz.com
                       foo@bar+baz.com
                      ]
    invalid_email.each do |invalid_email|
      @user.email = invalid_email
      assert_not @user.valid?, "#{invalid_email.inspect} should be invalid"
    end
  end

  test 'email validation should accept valid emails' do
    valid_email = %w[foosusan@example.com
                     foo_bar@gmail.cn
                     susan+goh@babaz.com
                     foo.bar@baz.AU
                    ]
    valid_email.each do |valid_email|
      @user.email = valid_email
      assert @user.valid?, "#{valid_email.inspect} should be valid"
    end
  end

  test 'password should have minimum length' do
    @user.password = @user.password_confirmation = 'a' * 7
    assert_not @user.valid?
  end
end
